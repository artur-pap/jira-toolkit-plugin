package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.comments.MockComment;
import com.atlassian.jira.mock.issue.MockIssue;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class LastCommentDateTest
{
    @Test
    public void getValueFromIssue_NoComments() throws Exception
    {
        final CommentManager commentManager = Mockito.mock(CommentManager.class);
        final MockIssue issue = new MockIssue(2L);
        final Timestamp createdTimestamp = new Timestamp(12893476);
        issue.setCreated(createdTimestamp);
        Mockito.when(commentManager.getComments(issue)).thenReturn(Collections.<Comment>emptyList());
        LastCommentDate lastCommentDate = new LastCommentDate(commentManager);

        final Date commentDate = lastCommentDate.getValueFromIssue(null, issue);
        assertEquals(createdTimestamp, commentDate);
    }

    @Test
    public void getValueFromIssue_Comments() throws Exception
    {
        final CommentManager commentManager = Mockito.mock(CommentManager.class);
        final MockIssue issue = new MockIssue(2L);
        final Timestamp createdTimestamp = new Timestamp(12893476);
        issue.setCreated(createdTimestamp);
        Mockito.when(commentManager.getComments(issue)).thenReturn(Arrays.<Comment>asList(
                new MockComment("fred", "Blah", null, null, new Date(1000000)),
                new MockComment("fred", "Blah", null, null, new Date(1000001)),
                new MockComment("fred", "Blah", null, null, new Date(1000002)),
                new MockComment("fred", "Blah", null, null, new Date(1000003)),
                new MockComment("fred", "Blah", null, null, new Date(1000004))
        ));
        LastCommentDate lastCommentDate = new LastCommentDate(commentManager);

        final Date commentDate = lastCommentDate.getValueFromIssue(null, issue);
        assertEquals(new Date(1000004), commentDate);
    }

    @Test
    public void getValueFromIssue_IgnorePrivateComments() throws Exception
    {
        final CommentManager commentManager = Mockito.mock(CommentManager.class);
        final MockIssue issue = new MockIssue(2L);
        final Timestamp createdTimestamp = new Timestamp(12893476);
        issue.setCreated(createdTimestamp);
        Mockito.when(commentManager.getComments(issue)).thenReturn(Arrays.<Comment>asList(
                new MockComment("fred", "Blah", null, null, new Date(1000000)),
                new MockComment("fred", "Blah", null, null, new Date(1000001)),
                new MockComment("fred", "Blah", "admins", null, new Date(1000002)),
                new MockComment("fred", "Blah", null, 2L, new Date(1000003)),
                new MockComment("fred", "Blah", "goats", null, new Date(1000004))
        ));
        LastCommentDate lastCommentDate = new LastCommentDate(commentManager);

        final Date commentDate = lastCommentDate.getValueFromIssue(null, issue);
        assertEquals(new Date(1000001), commentDate);
    }
}
